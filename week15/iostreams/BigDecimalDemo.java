package iostreams;

import java.math.BigDecimal;

public class BigDecimalDemo {

	public static void main(String[] args) {
		double d = 0.3;
		d = d * 3;
		System.out.println(d);

		BigDecimal b = new BigDecimal("0.3");
		b = b.multiply(new BigDecimal("3"));
		
		System.out.println(b);
	}

}
