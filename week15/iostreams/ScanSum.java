package iostreams;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Locale;

public class ScanSum {
	public static void main(String[] args) throws IOException {
		Scanner s = null;
		double sum = 0;
		try {
			s = new Scanner(new BufferedReader(new FileReader("week15/usnumbers.txt")));
			s.useLocale(new Locale("tr", "TR"));
			while (s.hasNext()) {
				if (s.hasNextDouble()) {
					sum += s.nextDouble();
				} else {
					String word = s.next();
					System.out.println(word);
				}
			}
		} finally {
			if(s != null)
				s.close();
		}
		System.out.println(sum);
	}
}
