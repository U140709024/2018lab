package iostreams;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.IOException;

public class CopyLines {
	public static void main(String[] args) throws IOException {
		BufferedReader inputStream = null;
		FileWriter outputStream = null;
		try {
			inputStream = new BufferedReader(new FileReader("week15/Xanadu.txt"));
			outputStream = new FileWriter("week15/characteroutputlines.txt");
			String l;
			while ((l = inputStream.readLine()) != null) {
				outputStream.write(l+"\n\n");
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
		}
	}
}