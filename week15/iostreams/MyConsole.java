package iostreams;

import java.io.Console;

public class MyConsole {

	public static void main(String[] args) {
		 Console cons;
		 char[] passwd;
		 if ((cons = System.console()) != null &&
		     (passwd = cons.readPassword("[%s]", "Password:")) != null) {
		     java.util.Arrays.fill(passwd, ' ');
		     
		 }else{
			 System.out.println("Console is null");
		 }
	}

}
