package iostreams;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyCharacters {
	public static void main(String[] args) throws IOException {
		FileReader inputStream = null;
		FileWriter outputStream = null;
		int charCount = 0;
		try {
			inputStream = new FileReader("week15/Xanadu.txt");
			outputStream = new FileWriter("week15/characteroutput.txt");
			int c;
			while ((c = inputStream.read()) != -1) {
				outputStream.write(c);
				charCount++;
			}
		} finally {
			if (inputStream != null) {
				inputStream.close();
			}
			if (outputStream != null) {
				outputStream.close();
			}
		}
		System.out.println(charCount + " characters");
	}
}
