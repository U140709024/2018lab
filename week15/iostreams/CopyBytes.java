package iostreams;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyBytes {

	public static void main(String[] args) throws IOException {
		FileInputStream in = null;
		FileOutputStream out = null;
		int byteCount = 0;
		try {
			in = new FileInputStream("week15/Xanadu.txt");
			out = new FileOutputStream("week15/outagain.txt");
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
				byteCount++;
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
		System.out.println(byteCount + " bytes");

	}

}