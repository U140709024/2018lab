package exceptions;

public class CallStackDemo {

	public static void main(String[] args) throws Exception {
		method1();
		System.out.println("main completed");

	}
	
	public static void method1() throws Exception{
		System.out.println("method1");
		try {
			method2();
		}catch(ArithmeticException ex) {
			System.out.println("resolved");
			throw new Exception(ex);
		}finally {
			System.out.println("method1 completed");
		}
	}

	
	public static void method2() {
		System.out.println("method2");
		method3();
		System.out.println("method2 completed");
	}
	
	public static void method3() {
		System.out.println("method3");
		try {
			int a = 5 / 0;
			//throw new NullPointerException();
		}finally {
			System.out.println("method3 completed");
		}
	}
	

}
