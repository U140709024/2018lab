package exceptions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class WordCounter {

	public static void main(String[] args) throws FileNotFoundException {
		WordCounter wordcounter = new WordCounter();
		try {
			wordcounter.countWords("sample.txt");
		} catch (FileNotFoundException fex) {
			// handle

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Successfull");

	}

	public int countWords(String fileName) throws IOException {
		int count = 0;
		BufferedReader br = null;
		try {
		br = new BufferedReader(
					new FileReader(fileName));
			
			String line = null;
			while((line = br.readLine()) !=null) {
			//some code
			}
		}finally {
			if(br != null)
				br.close();
		}
		return count;
	}

}
